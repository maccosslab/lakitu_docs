.. Lakitu documentation master file, created by
   sphinx-quickstart on Fri Jan  5 10:16:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Lakitu developer documentation!
==============================================

The Lakitu project is a computational infrastructure built to run mass spectrometry data processing pipelines on the cloud. A "pipeline" is a series of computational processing steps applied to an input data set (e.g. .raw files) to generate a desired result (e.g. a Skyline document). The project currently consists of the following components:

**AWS Infrastructure**
    Cloudformation scripts to build a scalable compute environment on the Amazon cloud which is used to execute pipeline components (`source <https://bitbucket.org/maccosslab/lakitu-cloudformation>`__).

**Dockerized Applications**
    The software for each step of a processing pipeline is stored in a Docker container. (`source <https://bitbucket.org/maccosslab/dockerapps>`__, `dockerhub <https://hub.docker.com/r/jegertso>`__).

**Local Command Line Client**
    A local command line client which can be used to list available pipelines (from a pipeline index) and run them on the cloud infrastructure. Files are synced between the local system and Amazon S3 (`source <https://bitbucket.org/maccosslab/lakitu>`__).

**Lakitu API**
    An python interface which must be implemented by each Lakitu pipeline and Lakitu pipeline executor to coordinate exchange of information about the location and credentials needed for accessing AWS infrastructure (`source <https://bitbucket.org/maccosslab/lakituapi>`__).

**DIA Library Query Pipeline**
    An example pipeline written to query DIA data using a spectral library (`source <https://bitbucket.org/maccosslab/lpencyclopedialibraryquery>`__).
    
Using this documentation
________________________
I highly recommended reading the :doc:`/overview` to get an idea of the structure of the entire project. Next, if you do not already have access to a working lakitu environment, you will want to check out the tutorial on creating the environment in :doc:`/tutorials`. If your goal is to do development work on any of the components, I recommend checking out the documentation for the specific component of interest.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   tutorials

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
