Overview
========

The complete Lakitu project has a lot of moving parts. This overview is designed to give a broad perspective of each component and how they interact.

The end result of all of this is that a user can run lakitu on their machine and something like the following:

.. Todo:: include command line examples of running lakitu

Pipelines
_________
The goal of the Lakitu project is to provide access to versioned, scalable pipelines. Here, a "pipeline" is a collection of inter-dependent processing steps that perform operations on an input data set. For example, a data processing pipeline for mass spectrometry data may look something like this:

.. image:: overview/pipeline.png

In this case, the input data is a set of raw data files, and a spectral or chromatogram library. Each processing step is in a colored box (e.g. **File Upload**, **Data Conversion**, etc.). Dependencies between processing steps are indicated with arrows. For example, the **Outlier Removal** step relies on inputs from the **Peptide Detection** step. Each step may have parameters associated with it. For example, the **Peptide Detection** step may have parameters to indicate a mass tolerance for the search. The processing steps in Lakitu pipelines communicate with each other via files. That is, if a processing step is to pass data to another step, it must do so by writing a file to disk (or Amazon S3). For example:

.. image:: overview/processing_dependencies.png

File outputs are used to monitor when processing steps are complete in Lakitu pipelines. Once a file output exists, the step that generated it is considered complete. Note, this means that file I/O must be atomic! More on that later.

Using Luigi to Specify Pipelines
________________________________
Lakitu pipelines are implemented as Python conda packages (`conda <https://conda.io/docs>`__) with source stored in a git repository and the Python package shared via an anaconda repository (`example <https://anaconda.org/jegertso>`__). Specifically, pipelines steps and dependencies are defined using the `Luigi <http://luigi.readthedocs.io/en/stable>`__ Python package. When a pipeline is to be run, the Lakitu command line client pulls down the Python pacakge from the online repository and runs it on the local machine using the Luigi pipeline executor.

Each processing step is defined as a subclass of the Luigi task class (`ref <http://luigi.readthedocs.io/en/stable/tasks.html>`__) in Python. The task class contains information on the parameters of the processing step, the other processing steps it depends on, the code to execute to "run" the task, and the expected output files ("targets") from successful execution of the task. With all steps of the pipeline specified as tasks, the Luigi pipeline executor proceeds to run each task once its dependencies are satisfied. A simple pipeline with two steps is shown below:

.. image:: overview/luigi_task_example.png

Source code::

    class GenerateWords(luigi.Task):
        def output(self):
            return luigi.LocalTarget('words.txt')

        def run(self):
            # write a dummy list of words to an output file
            words = ['apple', 'banana', 'grapefruit']

            with self.ouptut().open('w') as f:
                for word in words:
                    f.write('{word}\n'.format(word=word))

    class CountLetters(luigi.Task):
        def requires(self):
            return GenerateWords()

        def output(self): 
            return luigi.LocalTarget('letter_counts.txt')
        
        def run(self):
            # read in file as a list
            with self.input().open('r') as infile:
                words = infile.read().splitlines()

            # write each word to output file with its corresponding letter count
            with self.output().open('w') as outfile:
                for word in words:
                    outfile.write(
                        '{word} | {letter_count}\n'.format(
                            word=word, 
                            letter_count=len(word)
                            )
                    )

A "pipeline" is fundamentally a collection of processing steps defined as above in a Python module.

Software Components
___________________
Each individual processing step will rely on a piece of software to do the processing (e.g. EncyclopeDIA or Skyline). Software that is used in Lakitu pipelines is conatinerized using `Docker <https://www.docker.com/what-docker>`__. Containerization isolates the software from the compute environment it is run on. That is, a containerized piece of software will execute the same on any machine so long as the machine is running the Docker daemon and the same OS as the container. Crucially, all software dependencies of a given software component (e.g. libraries, interpreters, configuration files) are included in the container itself. Once a Docker container is built, the container image is uploaded to the public docker hub (`example <https://hub.docker.com/r/jegertso>`__). Once the container is on the docker hub, any particular version of the software may be downloaded and run by the docker daemon:

.. code-block:: console

    > docker run jegertso/encyclopedia:0.4.4-1

The above command will download the containerized software and provide the same command line interface as encyclopedia 0.4.4 without requiring installation of any of EncyclopeDIA's software dependencies (e.g. java). When lakitu pipelines are executed on the Amazon cloud, software components are automatically downloaded as needed.
Containerized software is wrapped with an adapter script that automatically synchronizes input and output of the software component with Amazon S3.
For example, EncyclopeDIA was not developed with S3 access; however, within the Lakitu execution environment, it may be executed with references to remote input and output files using this command:

.. code-block:: console

    > docker run jegertso/encyclopedia-0.4.4 s3://maccosslab-lakitudata/input.blib s3://maccosslab-lakitudata/raw/input.raw s3://maccosslab-lakitudata/output.txt

AWS Infrastructure
__________________
Heavy processing steps for Lakitu pipelines may be run on AWS. The lakitu project includes an Amazon CloudFormation script to create a compute environment on the cloud.
The compute environment consists of two scalable compute clusters: one Linux and one Windows. The clusters are ECS (EC2 Container Service) clusters meaning that
each node of the cluster runs the Docker daemon and accepts job submission which specify the Docker container containing the software needed to run the job in addition
to more common job submission parameters such as command line parameters and resource requirements. The clusters are automatically granted access to an S3 bucket specified on
creation which is used to store the inputs and outputs of processing steps as they complete. The AWS cloud infrastructure is diagrammed below:

.. image:: overview/aws_cloud.png


Running a Pipeline Containing AWS Compute Steps
_______________________________________________
Currently, even for pipelines containing cloud compute steps, the Luigi pipeline executor is run on the local machine.
Processing steps which specify cloud compute tasks are submitted as jobs to the remote cloud and monitored locally for completion.
If necessary or desired, intermediate processing files generated on the cloud may be downloaded automatically to the local machine.
A diagram of execution of a pipeline containing a Linux cloud step (imputation) and a Windows cloud step (Skyline) is shown below:

.. image:: overview/aws_execution.png

A Pipeline Index
________________
Currently, all Lakitu pipelines are Python packages which are stored on the Anaconda cloud (`example <https://anaconda.org/jegertso>`__).
To keep track of pipelines, it is useful to create an index file which stores the locations and versions of pipelines you or your group
may be interested in running. When this is done, the Lakitu command line application (see below) may list available pipelines for users
in your group and execute them. An example index file is below::

    PackageSources:
        - Type: conda_repository
          ChannelName: jegertso
          Pipelines:
            - PipelineName: lphelloworld
              PipelineVersions:
              - '0.0.2'
              - '0.0.3'
              - '0.0.3.dev1+gc9cf805'
              - '0.0.3.dev2+g5cbf081'

The above index file may be hosted on the web and the command line application configured to use this pipeline index (see below).

Tying it all Together: The Command Line Application
___________________________________________________

The Lakitu command line application (`git <https://bitbucket.org/maccosslab/lakitu>`__) is the way most users will run pipelines.
The application is a Python package which is configured on install with information on the AWS compute resources location and
access credentials, and the location of a pipeline index. To execute pipelines, this command line application must be installed,
have access to a local or remote instance of the luigi daemon (`more info <http://luigi.readthedocs.io/en/stable/central_scheduler.html>`__).

After configuration, a user may list the pipelines available to them using the *list* sub-command:

.. code-block:: console

    > lakitu list
    Pipeline:   lphelloworld
    Versions:   0.0.2
                0.0.3
                0.0.3.dev1+gc9cf805
                0.0.3.dev2+g5cbf081

They may then run a specific version of a pipeline using the *run* subcommand.

.. code-block:: console

    > lakitu run lphelloworld 0.0.3 --say hello
    hello

An administrator may "install" new pipelines for all users on the system by updating the pipeline index.

