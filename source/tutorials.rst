Tutorials
=========

:doc:`/tutorials/environment_setup`
    How to deploy a lakitu environment (AWS infrastructure, pipeline ilrepository, and local client install) from scratch.


.. toctree::
   :maxdepth: 1
   :hidden:

   tutorials/environment_setup
